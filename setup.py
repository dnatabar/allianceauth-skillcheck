# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from skillcheck import __version__

setup(
    name='skillcheck',
    description='Skillchecking Utility for Allianceauth',
    author='SpeedProg',
    author_email='speedprogde@googlemail.com',
    license='BSD 2-Clause',
    url='https://gitlab.com/speedprog/allianceauth-skillcheck',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'allianceauth>=2.0.4',  # this requires celery and django
    ],
    classifiers=[
          'License :: OSI Approved :: BSD License',
          'Natural Language :: English',
          'Programming Language :: Python :: 3.6',
          'Framework :: Django :: 1.11',
      ],
)
