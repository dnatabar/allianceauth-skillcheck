# SkillCheck

###### A skillchecker for allianceauth


#### Quick Install

1. Add `esi-skills.read_skills.v1` to allowed scopes for the app on the ccp developer site.
2. Add `skillcheck` to your `INSTALLED_APPS` in your Django config file.
3. Add settings from `settings.example.py` file to the Django config file.
4. Run `python manage.py migrate skillcheck` to apply migrations.
5. Run `python manage.py collectstatic` to move static files or move them yourself.
6. Restart Django and Celery.
7. Pull all the skill data from esi by running `celery -A <your_alliance_auth_project_name> call skillcheck.update_skill_data`
8. Add permissions to groups where required.

#### Permissions
###### character skill set
* Can load skillset for his character: This permission is required to view the tool, also allows loading skills for your character
* List this users characters in check selection: If a user has this permissions his characters show up for selection to check
* Can view skills of all characters: If a user has this permission he can view the skills of characters for check directly

###### skill set
* User can change skillsets: Users with this permission can change the skills in SkillSets and the SkillSets in Collections
* Can compare skillsets to characters and view the result: Can do comparisons of Collections against a Character
