from __future__ import unicode_literals
from django.db import models
import logging
from allianceauth.eveonline.models import EveCharacter

logger = logging.getLogger(__name__)


class Category(models.Model):
    class Meta:
        default_permissions = ()

    category_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=254)

    def __repr__(self):
        return f'<{self.__class__.__name__} name={self.name} id={self.category_id}>'


class Group(models.Model):
    class Meta:
        default_permissions = ()

    group_id = models.IntegerField(primary_key=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=254)
    published = models.BooleanField()

    def __repr__(self):
        return f'<{self.__class__.__name__} name={self.name} id={self.group_id}>'


class Skill(models.Model):
    class Meta:
        default_permissions = ()

    skill_id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    description = models.TextField()
    name = models.CharField(max_length=254, unique=True)
    published = models.BooleanField()

    def __repr__(self):
        return f'<{self.__class__.__name__} name={self.name} id={self.skill_id}>'


class SkillSet(models.Model):
    class Meta:
        permissions = (
            ('affect_skillset', 'User can change skillsets'),
            ('compare_skillset',
             'Can compare skillsets to characters and view the result'),
        )
        default_permissions = ()

    name = models.CharField(max_length=254)

    def __repr__(self):
        return f'<{self.__class__.__name__} name={self.name} id={self.id}>'


class SkillSetSkill(models.Model):
    class Meta:
        unique_together = (('set', 'skill'),)
        default_permissions = ()

    set = models.ForeignKey(SkillSet, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    level = models.PositiveSmallIntegerField()

    def __repr__(self):
        return f'<{self.__class__.__name__} id={self.id}>'


class SkillSetCollection(models.Model):
    class Meta:
        default_permissions = ()

    name = models.CharField(max_length=254)
    skillsets = models.ManyToManyField(SkillSet)

    def __repr__(self):
        return f'<{self.__class__.__name__} name={self.name} id={self.id}>'


class CharacterSkillSet(models.Model):
    class Meta:
        permissions = (
            ('affect_character_skillset',
             'Can load a skillset for his character'),
            ('view_character_skillset',
             'Can view skills of all characters'),
            ('list_in_check',
             'List this users characters in the check selection')
        )
        default_permissions = ()

    character = models.OneToOneField(EveCharacter, on_delete=models.CASCADE,
                                     related_name='+', primary_key=True)
    last_update = models.DateTimeField()

    def __repr__(self):
        return f'<{self.__class__.__name__} charname={self.character.character_name} id={self.id}>'


class CharacterSkillSetSkill(models.Model):
    class Meta:
        unique_together = (('set', 'skill'),)
        default_permissions = ()

    set = models.ForeignKey(CharacterSkillSet, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    level = models.PositiveSmallIntegerField()

    def __repr__(self):
        return f'<{self.__class__.__name__} id={self.id}>'
