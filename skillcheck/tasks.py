from celery.app import shared_task
from .managers.skills import SkillManager
import logging

logger = logging.getLogger(__name__)


@shared_task(name="skillcheck.update_skill_data")
def update_skill_data():
    logger.info("Running taks skillcheck.update_skill_data")
    SkillManager.update_skill_data()
    logger.info("Finished taks skillcheck.update_skill_data")
