from django.contrib import admin
from .models import CharacterSkillSet, CharacterSkillSetSkill
admin.site.register(CharacterSkillSet)
admin.site.register(CharacterSkillSetSkill)
