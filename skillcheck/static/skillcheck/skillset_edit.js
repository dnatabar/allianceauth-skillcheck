(function() {
	
	function loadSkillTree() {
		$.get({
			'url': urls.skill_list
		}).done(function(data, textStatus, jqXHR){
			let tree = [];
			
			// sort groups
			data.sort((ga, gb) => {
				let namea = ga.name.toUpperCase();
				let nameb = gb.name.toUpperCase();
				if (namea < nameb) {
					return -1;
				}
				if (namea > nameb) {
					return 1;
				}
				return 0;
			});
			
			// sort skills
			data.forEach(group => {
				group.skills.sort((sa, sb) => {
					let namea = sa.name.toUpperCase();
					let nameb = sb.name.toUpperCase();
					if (namea < nameb) {
						return -1;
					}
					if (namea > nameb) {
						return 1;
					}
					return 0;
				});
			});
			

			data = data.filter(group => {
				return group.published; // remove not published groups
			}).map(group => {
				// lets transform skills first
				
				group.skills = group.skills.filter(skill => {
					return skill.published; // remove not published skills
				}).map(skill => {
					return {
						'text': skill.name,
						'skill-id': skill.id,
					}
				});
				
				
				return {
					'text': group.name,
					'selectable': false,
					'nodes': group.skills
				}
			});
			
			$('#skilltree').treeview({
					data: data,
					levels: 0,
					multiSelect: true,
				});
		});
	}
	
	function handleRemove(event) {
		let target = event.currentTarget;
		let set_id = target.getAttribute('data-setid');
		let url = urls.skillset_remove_skill.replace('/0/', '/'+set_id+'/');
		let skill_id = target.getAttribute('data-skillid');
		$.post({
			'url': url,
			'data': {'skill_id': skill_id}
		}).then(
		function(data, textStatus, jqXHR) {
			let li_element = document.getElementById(`skill-${skill_id}`);
			li_element.remove();
		});
	}
	
	function handleSkillUpdate(event) {
		let target = event.currentTarget;
		let set_id = target.getAttribute('data-setid');
		let skilltree = $('#skilltree').data('treeview');
		let selected_skills = skilltree.getSelected();

		let url = urls.skillset_add_skill.replace('/0/', '/'+set_id+'/');
		let skill_level = document.getElementById('skilllevel').value;

		selected_skills.forEach(skill => {
			let skill_name = skill['text'];
			let skill_id = skill['skill-id'];
			$.post({
				'url': url,
				'data': {
					'skill_name': skill_name,
					'skill_level': skill_level,
					}
			}).done(
			function(data, textStatus, jqXHR) {
				let existingElement = document.getElementById(`skill-${skill_id}`);
				// if there is a row just update the level
				if (existingElement) {
					let level_node = existingElement.children[1];
					level_node.textContent = skill_level;
				} else {
					let table = document.getElementById('skilltable');
					let body = table.tBodies[0];
					let row = document.createElement('tr');
					row.setAttribute('id', `skill-${skill_id}`);
					let name_td = document.createElement('td');
					name_td.textContent = skill_name;
					let level_td = document.createElement('td');
					level_td.textContent = skill_level;
					let button_td = document.createElement('td');
					let button = document.createElement('button');
					button.setAttribute('class', 'btn btn-danger btn-xs');
					button.setAttribute('data-action', 'remove-skill');
					button.setAttribute('data-skillid', skill_id);
					button.setAttribute('data-setid', set_id);
					button.textContent = 'Remove';
					button_td.appendChild(button);
					row.appendChild(name_td);
					row.appendChild(level_td);
					row.appendChild(button_td);
					body.appendChild(row);
				}
				skilltree.unselectNode(skill, { silent: true });
			});
		});
	}
	
	function init() {
		let doc = $(document)
		doc.on('click', '[data-action="remove-skill"]', handleRemove);
		doc.on('click', '[data-action="update-skill"]', handleSkillUpdate);
		loadSkillTree();
	}
	$(document).ready(init);
})();