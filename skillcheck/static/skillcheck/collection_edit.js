(function() {
	function handleRemove(event) {
		let target = event.currentTarget;
		let url = target.getAttribute('data-url');
		let set_id = target.getAttribute('data-id');
		$.post({
			'url': url,
			'data': {'set_id': set_id}
		}).then(
		function(data, textStatus, jqXHR) {
			let li_element = document.getElementById(`set-${set_id}`);
			li_element.remove();
		});
	}
	
	function init() {
		$(document).on('click', '[data-action="remove-set"]', handleRemove);
	}
	$(document).ready(init);
})();