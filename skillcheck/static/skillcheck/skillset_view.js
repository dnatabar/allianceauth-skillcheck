(function() {
	function handleRemove(event) {
		let target = event.currentTarget;
		let url = target.getAttribute('data-url');
		let skillset_id = target.getAttribute('data-id');
		$.post({
			'url': url,
			'data': {'skillset_id': skillset_id}
		}).then(
		function(data, textStatus, jqXHR) {
			let li_element = document.getElementById(`skillset-${skillset_id}`);
			li_element.remove();
		});
	}
	
	function init() {
		$(document).on('click', '[data-action="remove-set"]', handleRemove);
	}
	$(document).ready(init);
})();