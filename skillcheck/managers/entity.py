from allianceauth.eveonline.models import EveCharacter
from allianceauth.authentication.models import CharacterOwnership
from ..models import CharacterSkillSet
from typing import List


class EveEntityManager:

    def __init__(self):
        pass

    @staticmethod
    def get_owner_from_character_id(character_id):
        """
        Attempt to get the character owner from the given character_id
        :param character_id: int character ID to get the owner for
        :return: User (django) or None
        """
        try:
            ownership = CharacterOwnership.objects.get(character__character_id=str(character_id))
            return ownership.user
        except CharacterOwnership.DoesNotExist:
            return None

    @staticmethod
    def get_characters_by_user(user):
        """Get all characters of the user"""
        return [owner_ship.character for owner_ship in CharacterOwnership.objects.filter(user=user)]

    @staticmethod
    def is_character_owned_by_user(character_id, user):
        """Check if character with the given id is owned by the given user"""
        try:
            CharacterOwnership.objects.get(user=user,character__character_id=character_id)
            return True
        except CharacterOwnership.DoesNotExist:
            return False

    @staticmethod
    def get_characters_with_skillset() -> List[EveCharacter]:
        """Get characters that have a skillset"""
        return [skillset.character
                for skillset in CharacterSkillSet.objects.all()]

    @classmethod
    def get_character_for_check_selection(cls) -> List[EveCharacter]:
        """Get all characters that should be displayed on the check selection
        """
        return [char for char in cls.get_characters_with_skillset()
                if cls.should_show_for_check(char)]

    @classmethod
    def should_show_for_check(cls, char: EveCharacter) -> bool:
        """Should the given character show up for chat selection"""
        owner = cls.get_owner_from_character_id(char.character_id)
        if owner is None:
            return False

        return owner.has_perm('skillcheck.list_in_check')
