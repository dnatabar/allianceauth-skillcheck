import os

SWAGGER_SPEC_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                 '..', 'swagger.json')
