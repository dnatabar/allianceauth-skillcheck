from django.db import transaction
from . import SWAGGER_SPEC_PATH
from esi.clients import esi_client_factory
from bravado.client import SwaggerClient
from ..models import Skill, SkillSet, Category, Group, CharacterSkillSet,\
 CharacterSkillSetSkill, SkillSetCollection
from allianceauth.eveonline.models import EveCharacter
from django.utils import timezone
from typing import Dict, List, Optional, Tuple
from esi.models import Token
import logging
from skillcheck.models import SkillSetSkill

logger = logging.getLogger(__name__)


class SkillManager(object):
    SKILL_CATEGORY_ID = 16

    @staticmethod
    def api_get_instance(token: Optional[Token]=None) -> SwaggerClient:
        """Get an api instance
        If a token is given it is used for authorization by the api.
        """
        if token is None:
            return esi_client_factory(spec_file=SWAGGER_SPEC_PATH)
        return esi_client_factory(token=token, spec_file=SWAGGER_SPEC_PATH)

    @classmethod
    @transaction.atomic
    def update_skill_data(cls) -> None:
        """Updates database skill data.
        New types are added, but old types(byid) are NOT removed
         if they don't exist anymore.
        """
        logger.info('Skilldata Updating')
        api: SwaggerClient = cls.api_get_instance()

        try:
            category = Category.objects.get(
                category_id=SkillManager.SKILL_CATEGORY_ID)
        except Category.DoesNotExist:
            category = Category(category_id=SkillManager.SKILL_CATEGORY_ID,
                                name='Skills')
            category.save()
        logger.debug('Created category %r', category)

        category_result = api.Universe.get_universe_categories_category_id(
            category_id=category.category_id).result()
        for group_id in category_result['groups']:
            group_result = api.Universe.get_universe_groups_group_id(
               group_id=group_id) .result()

            try:
                group = Group.objects.get(group_id=group_id)
            except Group.DoesNotExist:
                group = Group(group_id=group_id,
                              category=category,
                              name=group_result['name'],
                              published=group_result['published'])
                group.save()
            logger.info('Created Group %r', group)

            for type_id in group_result['types']:
                type_result = api.Universe.get_universe_types_type_id(
                    type_id=type_id).result()
                skill, created = Skill.objects.update_or_create(
                    skill_id=type_result['type_id'],
                    defaults={
                        'skill_id': type_result['type_id'],
                        'group': group,
                        'description': type_result['description'],
                        'name': type_result['name'],
                        'published': type_result['published']
                        }
                    )
                if created:
                    logger.debug('Created Skill %r', skill)
                else:
                    logger.debug('Updated Skill %r', skill)
        logger.info('Skilldata Updated')

    @classmethod
    @transaction.atomic
    def skillset_create(cls, name: str) -> SkillSet:
        """Create a new SkillSet with the given name
        """
        skillset = SkillSet(name=name)
        skillset.save()
        return skillset

    @staticmethod
    @transaction.atomic
    def skillset_remove(skillset_id: int) -> None:
        """Remove the skillset with the given id"""
        SkillSet.objects.get(id=skillset_id).delete()

    @staticmethod
    @transaction.atomic
    def skillset_add_skill(skill_name: str, skillset_id: int,
                           skill_level: int) -> None:
        """Add skill with the given name and level to the set"""
        skillset = SkillSet.objects.get(id=skillset_id)
        skill = Skill.objects.get(name=skill_name)
        SkillSetSkill.objects.update_or_create(
            set=skillset,
            skill=skill,
            defaults={'level': skill_level})

    @classmethod
    @transaction.atomic
    def collection_create(cls, name: str) -> SkillSetCollection:
        """Create a collection with the given name"""
        collection = SkillSetCollection(name=name)
        collection.save()
        return collection

    @staticmethod
    @transaction.atomic
    def collection_remove(collection_id: int) -> None:
        """Remove the collection with the given id"""
        SkillSetCollection.objects.get(id=collection_id).delete()

    @staticmethod
    @transaction.atomic
    def collection_remove_set(collection_id: int, set_id: int) -> None:
        """Remove a SkillSet from the SkillSetCollection
        :param collection_id: id of the collection to remove the set from
        :param set_id: id of the set to remove
        """
        collection = SkillSetCollection.objects.get(id=collection_id)
        skillset = SkillSet.objects.get(id=set_id)
        collection.skillsets.remove(skillset)

    @staticmethod
    @transaction.atomic
    def collection_add_set(collection_id: int, set_name: str) -> None:
        """Add set with the given name to the collection"""
        skillset = SkillSet.objects.get(name=set_name)
        collection: SkillSetCollection = SkillSetCollection.objects.get(
            id=collection_id)
        collection.skillsets.add(skillset)

    @classmethod
    @transaction.atomic
    def update_skills_for_character(cls, character_id: int, token: Token) -> None:
        """Load the current skills of the character
        If it ever happens that skillbooks can be extracted
         this needs to be updated to remove none existand skills
        """
        logger.info('Skills for character with id=%d updating', character_id)
        character = EveCharacter.objects.get(character_id=character_id)

        skillset, _ = CharacterSkillSet.objects.get_or_create(
            character=character,
            defaults={
                'character': character,
                'last_update': timezone.now()
                })
        client = cls.api_get_instance(token)

        skill_result = client.Skills.get_characters_character_id_skills(
            character_id=character_id).result()

        for skill in skill_result['skills']:
            # skill_id
            # trained_skill_level
            skill_id = skill['skill_id']
            skill_level = skill['trained_skill_level']
            db_skill = Skill.objects.get(skill_id=skill_id)
            CharacterSkillSetSkill.objects.update_or_create(
                set=skillset,
                skill=db_skill,
                defaults={
                    'level': skill_level
                })
        logger.info('Skills for character with id=%d finished updating', character_id)

    @staticmethod
    def get_missing_requirements(
            character_skills: Dict[int, int],
            skillset: SkillSet) -> List[Tuple[str, int, int]]:
        """Get skills that are not meet
        :param character_skills: A dictionary with the key being the skill id
         and the value the skills level
        :param skillset: SkillSet that the character should be checked against
        :return A list of tuples (skill_name, needs, has)
        """
        missing: List[Tuple[str, int, int]] = []
        for setskill in skillset.skillsetskill_set.all():
            if setskill.skill_id in character_skills:
                if setskill.level <= character_skills[setskill.skill_id]:
                    continue
                missing.append((setskill.skill.name, setskill.level,
                                character_skills[setskill.skill_id]))
            else:
                missing.append((setskill.skill.name, setskill.level, 0))
        return missing

    @staticmethod
    def get_character_skill_dict_from_set(
            character_skillset: CharacterSkillSet) -> Dict[int, int]:
        """Get a skill dictionary  from the CharacterSkillSet
        :return A dictionary where the key is the skill id
         and the value the skill level
        """
        skills = dict()
        for char_skill in character_skillset.characterskillsetskill_set.all():
            skills[char_skill.skill_id] = char_skill.level
        return skills
