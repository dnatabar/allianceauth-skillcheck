from __future__ import unicode_literals

from .urls import urlpatterns

import logging
from allianceauth import hooks
from allianceauth.services.hooks import ServicesHook, MenuItemHook

logger = logging.getLogger(__name__)


class SkillCheckService(ServicesHook):
    def __init__(self):
        ServicesHook.__init__(self)
        self.name = 'skillcheck'
        self.urlpatterns = urlpatterns
        self.access_perm = 'skillcheck.affect_character_skillset'

    def delete_user(self, user, notify_user=False):
        """ Removes user data
        We don't store any data about users
        Character related data should be delete by on_delete=CASCADE
        """
        logger.debug('Deleting user %s data', user)

    def validate_user(self, user):
        """Validating of a user still has access to the service
        If he has no access anymore delete his data
        """
        logger.debug('Validating user %s data', user)
        if not self.service_active_for_user(user):
            self.delete_user(user)

    def service_active_for_user(self, user):
        """Check if the service is enabled for the given user"""
        return user.has_perm(self.access_perm)


@hooks.register('services_hook')
def register_service():
    """Register our service with allianceauth"""
    return SkillCheckService()


class SkillCheckMenuItem(MenuItemHook):
    def __init__(self):
        MenuItemHook.__init__(self,
                              'Skill Check',
                              'fa  fa-bars fa-fw',
                              'skillcheck:view_index')

    def render(self, request):
        """Render the menu entry if the user has access"""
        if request.user.has_perm('skillcheck.affect_character_skillset'):
            return MenuItemHook.render(self, request)
        return ''


@hooks.register('menu_item_hook')
def register_menu():
    """Register our menu entry with allianceauth"""
    return SkillCheckMenuItem()
