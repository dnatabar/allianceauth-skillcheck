if 'skillcheck' in INSTALLED_APPS:
    CELERYBEAT_SCHEDULE['skillcheck_update_skill_data'] = {
        'task': 'skillcheck.update_skill_data',
        'schedule': crontab(hour='*/24', minute='45'),
    }
