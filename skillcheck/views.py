from django.contrib.auth.decorators import login_required, permission_required
import logging
from .models import SkillSet, Skill, SkillSetSkill, CharacterSkillSet, SkillSetCollection
from django.shortcuts import render, redirect
from skillcheck.managers.skills import SkillManager
from esi.decorators import token_required
from .managers.entity import EveEntityManager
from django.views.decorators.http import require_http_methods
from allianceauth.eveonline.models import EveCharacter
from django.http.response import HttpResponse
from django.contrib import messages
from skillcheck.models import Group
from esi.models import Token
from pip._vendor.pkg_resources import require
from typing import Iterable
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.cache import cache

logger = logging.getLogger(__name__)


@login_required
@permission_required('skillcheck.affect_character_skillset')
def view_index(request):
    characters = EveEntityManager.get_characters_by_user(request.user)
    return render(request, 'skillcheck/index.html',
                  {'characters': characters})


@login_required
@permission_required('skillcheck.affect_skillset')
def skillsets_view(request):
    skillsets = SkillSet.objects.all()
    return render(request, 'skillcheck/skillset.html', {
        'skillsets': skillsets,
        })


@login_required
@permission_required('skillcheck.affect_skillset')
def skillset_edit_view(request, skillset_id: int):
    return render(request, 'skillcheck/skillset_edit.html',
                  {'skillset': SkillSet.objects.get(id=skillset_id),
                   'skills': Skill.objects.order_by('name').all()
                   })


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def skillset_add(request):
    name = request.POST.get('setname')
    SkillManager.skillset_create(name)
    return redirect('skillcheck:skillsets_view')


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def skillset_remove(request):
    skillset_id = request.POST['skillset_id']
    SkillManager.skillset_remove(skillset_id)
    return HttpResponse(status=204)


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def skillset_remove_skill(request, skillset_id: int):
    skill_id = request.POST['skill_id']
    setskill = SkillSetSkill.objects.get(set__id=skillset_id,
                                         skill__skill_id=skill_id)
    if setskill is not None:
        setskill.delete()
    return HttpResponse('', status=204)


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def skillset_add_skill_view(request, skillset_id: int):
    skill_name = request.POST['skill_name']
    skill_level = int(request.POST['skill_level'])

    SkillManager.skillset_add_skill(skill_name, skillset_id, skill_level)

    return redirect('skillcheck:skillset_edit_view', skillset_id=skillset_id)


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_character_skillset')
@token_required('esi-skills.read_skills.v1')
def character_skills_update(request, token: Token, character_id: int):
    SkillManager.update_skills_for_character(character_id, token)
    messages.add_message(request, messages.SUCCESS, 'Skills Imported!')
    return redirect('skillcheck:character_skills_show_view',
                    character_id=character_id)


@login_required
@permission_required('skillcheck.affect_character_skillset')
def character_skills_show_view(request, character_id: int):
    if (
            (not request.user.has_perm('skillcheck.view_character_skillset'))
            and EveEntityManager.get_owner_from_character_id(
                character_id).id != request.user.id
    ):
        return HttpResponse(
            content=('You are not allowed to view'
                     ' the skills of this character.'),
            status=403)
    skillset = CharacterSkillSet.objects.get(
        character__character_id=character_id)

    groups_db = Group.objects.order_by('name').all()
    groups = dict()

    for group in groups_db:
        if group.published:
            groups[group.group_id] = [group.name, []]

    for skill in skillset.characterskillsetskill_set.all():
        groups[skill.skill.group_id][1].append(skill)

    return render(request, 'skillcheck/character_skills.html',
                  {'groups': groups})


@login_required
@permission_required('skillcheck.affect_skillset')
def collections_view(request):
    return render(request, 'skillcheck/collections.html',
                  {'collections': SkillSetCollection.objects.all()})


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def collection_add(request):
    name = request.POST.get('collection_name')
    SkillManager.collection_create(name)
    return redirect('skillcheck:collections_view')


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def collection_remove(request):
    collection_id = request.POST.get('collection_id')
    SkillManager.collection_remove(collection_id)
    return HttpResponse(status=204)


@login_required
@permission_required('skillcheck.affect_skillset')
def collection_edit_view(request, collection_id: int):

    return render(request, 'skillcheck/collection_edit.html',
                  {'collection':
                   SkillSetCollection.objects.get(id=collection_id),
                   'skillsets': SkillSet.objects.order_by('name').all()})


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def collection_add_set(request, collection_id: int):
    skillset_name = request.POST['set_name']
    SkillManager.collection_add_set(collection_id, skillset_name)
    return redirect('skillcheck:collection_edit_view',
                    collection_id=collection_id)


@login_required
@require_http_methods(["POST"])
@permission_required('skillcheck.affect_skillset')
def collection_remove_set(request, collection_id: int):
    set_id = request.POST['set_id']
    SkillManager.collection_remove_set(collection_id, set_id)
    return HttpResponse(status=204)


@login_required
@require_http_methods(["GET"])
@permission_required('skillcheck.compare_skillset')
def check_character_view(request):
    character_id = int(request.GET['character_id'])
    collection_id = int(request.GET['collection_id'])
    character = EveCharacter.objects.get(character_id=character_id)

    # should this character be allowed to be checked
    if (not EveEntityManager.should_show_for_check(character)):
        return HttpResponse(
            content='This character is not allowed to be checked',
            status=403)

    collection = SkillSetCollection.objects.get(id=collection_id)
    character_skills = CharacterSkillSet.objects.get(
        character__character_id=character_id)
    skill_dict = SkillManager.get_character_skill_dict_from_set(
        character_skills)

    results = []
    for skillset in collection.skillsets.all():
        results.append((skillset.name,
                        SkillManager.get_missing_requirements(
                            skill_dict, skillset)))

    return render(request, 'skillcheck/collection_result.html',
                  {'character': character,
                   'collection': collection,
                   'results': results})


@login_required
@require_http_methods(["GET"])
@permission_required('skillcheck.compare_skillset')
def check_character_selection_view(request):
    characters = EveEntityManager.get_character_for_check_selection()
    collections = SkillSetCollection.objects.all()
    return render(request, 'skillcheck/check_selection.html',
                  {'characters': characters,
                   'collections': collections})


@login_required
@require_http_methods(['GET'])
@permission_required('skillcheck.affect_skillset')
def download_skills(request):

    def get_skill_list_json():
        groups_list = []
        groups = Group.objects.all()

        for group in groups:
            skill_list = []
            for skill in group.skill_set.all():
                skill_list.append({
                    'id': skill.skill_id,
                    'name': skill.name,
                    'published': skill.published,
                    'group_id': skill.group_id,
                    'desc': skill.description,
                    })
            group_obj = {
                'id': group.group_id,
                'name': group.name,
                'published': group.published,
                'skills': skill_list
                }
            groups_list.append(group_obj)

        return json.dumps(groups_list, cls=DjangoJSONEncoder)

    skill_list_json = cache.get_or_set('skillcheck_skill_list',
                                       get_skill_list_json, timeout=3600)
    return HttpResponse(content=skill_list_json, content_type='application/json')
