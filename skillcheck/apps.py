from django.apps import AppConfig


class SkillcheckConfig(AppConfig):
    name = 'skillcheck'
